import * as React from 'react';

interface PropsType {
  color: String;
}

interface StateType {
  getUser?: () => String,
  name: String
}

class Test extends React.Component<PropsType, StateType> {
  constructor(props: PropsType) {
    super(props);
    this.state = {name: 'Jmeno'};
  }

  getUser() {
    const {name} = this.state;

    return name;
  }

  render() {
    return (
      <div>
        {this.getUser()}
      </div>
    );
  }
}

export default Test;
