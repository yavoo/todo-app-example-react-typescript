import * as React from 'react';
import * as Radium from 'radium';

export interface TodoItemProp {
  id: number;
  text: string;
  done: boolean;
}

interface ItemProps {
  item: TodoItemProp;
  remove: Function;
  togle: Function;
}

const styles = {
  wrapper: {
    borderBottom: '1px dashed #eee',
    display: 'flex',
    padding: '8px 0',
    ':hover': {}
  },
  content: {
    flex: '1 0 auto'
  },
  remove: {
    color: '#D88E81',
    cursor: 'pointer',
    display: 'none',
    fontSize: '18px'
  },
  removeHover: {
    display: 'block'
  },
  mark: {
    wrapper: {
      cursor: 'pointer',
      marginRight: '8px'
    },
    done: {
      backgroundColor: 'white',
      border: '1px solid #62B347',
      color: '#62B347',
      fontSize: '18px',
      height: '20px',
      textAlign: 'center',
      width: '20px'
    },
    open: {
      backgroundColor: 'white',
      border: '1px solid #ddd',
      height: '20px',
      width: '20px'
    }
  }
};

class TodoItem extends React.Component<ItemProps, null> {

  renderStatus() {
    const {item: {done}} = this.props;

    return (
      <div onClick={this.togle.bind(this)} style={styles.mark.wrapper}>
        {done && <div style={styles.mark.done}>✓</div>}
        {!done && <div style={styles.mark.open} />}
      </div>
    );
  }

  renderRemove() {
    const hover = Radium.getState(this.state, 'item', ':hover');

    return (
      <div
        onClick={this.remove.bind(this)}
        style={[styles.remove, hover && styles.removeHover]}
      >
        ✗
      </div>
    );
  }

  render() {
    const {item: {text}} = this.props;

    return (
      <li style={styles.wrapper} ref="item">
        {this.renderStatus()}
        <div style={styles.content}>
          {text}
        </div>
        {this.renderRemove()}
      </li>
    );
  }

  remove() {
    const {remove, item: {id}} = this.props;

    remove(id);
  }

  togle() {
    const {togle, item: {id}} = this.props;

    togle(id);
  }
}

export default Radium(TodoItem);
