import * as React from 'react';
import * as ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import Button from './Button';
import ListItem, {TodoItemProp} from './TodoItem';
import {Style} from 'radium';

interface TodoListState {
  list: Object[];
}

const styles = {
  wrapper: {
    backgroundColor: '#fff',
    borderRadius: '8px',
    boxShadow: '0 2px 6px rgba(0, 0, 0, .15)',
    margin: '16px auto',
    maxWidth: '420px',
    overflow: 'hidden',
    padding: '16px',
    textAlign: 'left'
  },
  heading: {
    borderBottom: '1px solid #F0C487',
    color: '#424F50',
    fontSize: '32px',
    margin: '8px 0'
  },
  list: {
    listStyle: 'none',
    padding: 0
  },
  form: {
    backgroundColor: '#424F50',
    display: 'flex',
    margin: '16px -16px -16px -16px',
    padding: '16px'
  },
  input: {
    background: 'none',
    borderColor: '#F0C487',
    borderWidth: '0 0 1px 0',
    color: 'white',
    flex: '1 0 auto',
    fontSize: '18px',
    marginRight: '12px',
    outline: 'none',
    padding: '8px'
  }
};

class TodoList extends React.Component<null, TodoListState> {

  state = {
    list: Array()
  };

  private textInput: HTMLInputElement;

  renderList() {
    const {list} = this.state;

    return (
      <ul style={styles.list}>
        <ReactCSSTransitionGroup
          transitionName="todolist"
          transitionEnterTimeout={500}
          transitionLeaveTimeout={300}
        >
          {list.map(
            item => <ListItem
              item={item}
              key={item.id}
              remove={this.remove.bind(this)}
              togle={this.togle.bind(this)}
            />
          )}
        </ReactCSSTransitionGroup>
      </ul>
    );
  }

  renderForm() {
    return (
      <div style={styles.form}>
        <input
          style={styles.input}
          type="text"
          ref={input => this.textInput = input}
          placeholder="place your text"
        />
        <Button onClick={this.add.bind(this)}>add new</Button>
      </div>
    );
  }

  renderStyles() {
    return (
      <Style rules={{
        '.todolist-enter': {
          opacity: 0.01,
          maxHeight: '0px',
        },
        '.todolist-enter.todolist-enter-active': {
          opacity: 1,
          maxHeight: '100px',
          transition: 'all 500ms ease-in'
        },
        '.todolist-leave': {
          opacity: 1,
          maxHeight: '100px',
        },
        '.todolist-leave.todolist-leave-active': {
          opacity: 0.01,
          maxHeight: '0px',
          transition: 'all 300ms ease-in'
        }
      }} />
    );
  }

  render() {

    return (
      <div style={styles.wrapper}>
        {this.renderStyles()}
        <h1 style={styles.heading}>Todo list</h1>
        {this.renderList()}
        {this.renderForm()}
      </div>
    );
  }

  add() {
    const {list} = this.state;
    const newItem: TodoItemProp = {
      id: Math.floor(Math.random() * 1000),
      text: this.textInput.value,
      done: false
    };
    const updatedList: Object[] = [...list, newItem];

    this.textInput.value = '';
    this.setState({'list': updatedList});
  }

  remove(id: number) {
    const {list} = this.state;
    const updatedList = list.filter(item => item.id !== id);

    this.setState({'list': updatedList});
  }

  togle(id: number) {
    const {list} = this.state;
    const updatedList = list.map(item => {
      if (item.id === id) {
       item.done = !item.done;
      }
      return item;
    });

    this.setState({'list': updatedList});
  }
}

export default TodoList;
