import * as React from 'react';

interface ButtonProps {
  onClick: Function;
}

const styles = {
  base: {
    backgroundColor: '#F0C487',
    borderRadius: '4px',
    border: '0',
    color: '#424F50',
    cursor: 'pointer',
    outline: 'none',
    padding: '12px 16px',
    textTransform: 'uppercase'
  }
};

class Button extends React.Component<ButtonProps, null> {

  render() {
    const {children, onClick} = this.props;

    return (
      <button onClick={onClick.bind(this)} style={styles.base}>
        {children}
      </button>
    );
  }
}

export default Button;
